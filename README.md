# Scripts
A collection of scripts that help make life easier in the terminal.

## Requirements
Some scripts require the following to be installed and in your system path:
- curl
- feh
- ffmpeg (with Xcb support)
- fzf
- neovim
- tiddlywiki (node.js package)
- zpool
- $EDITOR environment variable set
